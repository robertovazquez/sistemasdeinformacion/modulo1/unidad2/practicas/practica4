﻿USE roberpractica4;

-- ejercicio 1
     
     SELECT c.dni FROM cliente c;

 -- ejercicio 2

    SELECT * FROM programa p;

 -- ejercicio 3

     SELECT DISTINCT p.nombre FROM programa p;

-- 4 ejercicio

SELECT c.cif,c.nombre,c.ciudad FROM comercio c;

  -- 5 ejercicio 

     SELECT DISTINCT c.nombre FROM comercio;

 -- 6 ejercicio
   
      SELECT DISTINCT p.nombre FROM programa p;

  -- 7 ejercicio

      SELECT (c.dni+4) dni FROM cliente c;

   -- 8 ejercicio 

       SELECT (p.codigo*7) codigo FROM programa p;

   -- 9 ejercicio

      SELECT DISTINCT p.codigo FROM programa p WHERE p.codigo<=10;

   -- 10 ejercicio 
      
       SELECT p.nombre FROM programa p WHERE p.codigo=11;

    -- 11 ejercicio
      
      SELECT f.nombre FROM fabricante f WHERE f.pais='Estados Unidos'; 

    -- 12 ejercicio

       SELECT DISTINCT f.nombre FROM fabricante f WHERE f.pais NOT IN ('España');



-- 13 ejercicio

    SELECT p.version FROM programa p WHERE p.nombre='WINDOWS';

    -- 14 ejercicio 

    SELECT f.pais FROM fabricante f WHERE f.nombre='Corte Ingles'; 


    -- 15 ejercicio

      SELECT c.nombre FROM comercio c WHERE c.nombre NOT IN ('El Corte Inglés');


   -- 16 ejercicio

     

    -- 17 ejercicio

    SELECT c.nombre FROM cliente c WHERE  c.edad BETWEEN 10 AND 25 OR c.edad>50;

    -- 18 ejercicio

    SELECT DISTINCT c.nombre FROM comercio c WHERE c.ciudad='Sevilla' OR c.ciudad='Madrid';

    -- 19 ejercicio

     SELECT c.nombre FROM cliente c WHERE c.nombre LIKE  '%o';

    -- 20 ejercicio

       SELECT c.nombre FROM cliente c WHERE c.nombre LIKE  '%o' AND c.edad>30;

      -- 21 ejercicio

         SELECT p.nombre FROM programa p WHERE p.version LIKE '%i' OR p.nombre LIKE 'A%' OR p.nombre LIKE 'W%';

     -- 20 ejercicio

  SELECT c.nombre  FROM cliente c  WHERE c.nombre LIKE '%o' AND c.edad>30;

-- 21 ejercicio

  SELECT p.version, p.nombre   FROM programa p  WHERE p.version LIKE '%i'  OR p.nombre LIKE 'A%'  OR p.nombre LIKE 'W%';

-- 22 ejercicio

  SELECT p.version, p.nombre  FROM programa p WHERE p.version LIKE '%i'  OR p.nombre LIKE 'A%' AND p.nombre LIKE '%S';

-- 23 ejerccio

  SELECT p.version, p.nombre  FROM programa p  WHERE p.version LIKE '%i'  AND p.nombre NOT LIKE 'A%';

-- 24 ejercicio

  SELECT DISTINCT f.nombre FROM fabricante f  ORDER BY f.nombre ASC;

-- 25ejercicio

  SELECT DISTINCT f.nombre FROM fabricante f ORDER BY f.nombre DESC;

-- 26 ejercicio

  SELECT DISTINCT p.version  FROM programa p  ORDER BY p.version ASC;

-- 27 ejercicio

  -- C1 
  SELECT f.id_fab  FROM fabricante f  WHERE f.nombre='Oracle';

  -- Consulta final
  SELECT DISTINCT p.nombre  FROM desarrolla d 
    JOIN programa p 
    ON d.codigo = p.codigo 
    WHERE d.id_fab=(
      SELECT f.id_fab 
        FROM fabricante f 
        WHERE f.nombre='Oracle');

-- 28 ejercicio

  -- C1 
  SELECT p.codigo 
    FROM programa p 
    WHERE p.nombre = 'Windows';

  -- Consulta final
  SELECT c.nombre   FROM distribuye d 
    JOIN comercio c 
    ON d.cif = c.cif 
    WHERE d.codigo IN (
      SELECT p.codigo 
        FROM programa p 
        WHERE p.nombre = 'Windows');

-- 29 ejercicio

  -- C1 - 
  SELECT c.cif  FROM comercio c  WHERE c.nombre = 'El Corte Ingles'  AND c.ciudad = 'Madrid';

  -- Consulta final
  SELECT p.nombre, p.version, d.cantidad   FROM programa p 
   JOIN distribuye d 
    JOIN (
      SELECT c.cif 
        FROM comercio c 
        WHERE c.nombre = 'El Corte Ingles' 
        AND c.ciudad = 'Madrid'
      )c1 
      ON p.codigo = d.codigo 
      AND d.cif = c1.cif;

-- 30 

  -- C1 
  SELECT p.codigo  FROM programa p   WHERE p.nombre ='Freddy Hardest';

  -- Consulta final
  SELECT f.nombre  FROM fabricante f 
    JOIN desarrolla d 
    JOIN (  SELECT p.codigo  FROM programa p  WHERE p.nombre ='Freddy Hardest' )c1 
    ON f.id_fab = d.id_fab 
    AND d.codigo = c1.codigo;

-- 31 ejercicio

  SELECT p.nombre  FROM registra r  JOIN programa p  ON r.codigo = p.codigo  WHERE r.medio = 'Internet';

-- 32 ejercicio

  SELECT c.nombre FROM registra r  JOIN cliente c  ON r.dni = c.dni WHERE r.medio = 'Internet';

-- 33 ejercicio

  -- C1 
  SELECT c.dni  FROM cliente c WHERE c.nombre = 'Pepe Pérez';

  -- Consulta final
  SELECT r.medio  FROM registra r 
    JOIN (
      SELECT c.dni 
        FROM cliente c 
        WHERE c.nombre = 'Pepe Pérez'
    )c1 
    ON r.dni = c1.dni;

-- 34 ejercicio

  SELECT c.nombre  FROM registra r  JOIN cliente c  ON r.dni = c.dni  WHERE r.medio = 'Internet';

-- 35 ejercicio

  SELECT p.nombre, p.version  FROM registra r  JOIN programa p  ON r.codigo = p.codigo  WHERE r.medio='Tarjeta Postal';

-- 36 ejercicio

  SELECT c.ciudad  FROM registra r JOIN comercio c  ON r.cif = c.cif  WHERE r.medio = 'Internet';

  -- 37 ejercicio

  SELECT c.nombre AS cliente, p.nombre, p.version 
    FROM cliente c 
    JOIN registra r 
    JOIN programa p 
    ON c.dni = r.cif 
    AND r.codigo = p.codigo 
    WHERE r.medio='Internet';

-- 38 ejerccio 

  
  SELECT DISTINCT c.nombre, c.dni 
    FROM cliente c 
    JOIN registra r 
    ON c.dni = r.dni;

  
  SELECT c1.nombre cliente, p.nombre, p.version, p.codigo 
    FROM programa p 
    JOIN registra r 
    JOIN (
      SELECT DISTINCT c.nombre, c.dni 
        FROM cliente c 
        JOIN registra r 
        ON c.dni = r.dni
      )c1 
    ON p.codigo = r.codigo 
    AND c1.dni = r.dni;

  -- consulta final 
  SELECT c2.cliente, c2.nombre, c2.version, c.nombre comercio, r.medio 
    FROM comercio c 
    JOIN registra r 
    JOIN (
      SELECT c1.nombre cliente, p.nombre, p.version, p.codigo 
        FROM programa p 
        JOIN registra r 
        JOIN (
          SELECT DISTINCT c.nombre, c.dni 
            FROM cliente c 
            JOIN registra r 
            ON c.dni = r.dni
          )c1 
        ON p.codigo = r.codigo 
        AND c1.dni = r.dni
      )c2 
    ON c.cif = r.cif 
    AND r.codigo = c2.codigo;

-- 39 ejercicio

  -- C1 
  SELECT f.id_fab 
    FROM fabricante f 
    WHERE f.nombre='Oracle';

  -- C2 
  SELECT p.codigo 
    FROM desarrolla d 
    JOIN programa p 
    ON d.codigo = p.codigo 
    WHERE d.id_fab=(
      SELECT f.id_fab 
        FROM fabricante f 
        WHERE f.nombre='Oracle');

  -- Consulta final
  SELECT DISTINCT c.ciudad 
    FROM programa p 
    JOIN distribuye d 
    JOIN comercio c 
    ON p.codigo = d.codigo 
    AND d.cif = c.cif 
    WHERE p.codigo 
    IN (
      SELECT p.codigo 
        FROM desarrolla d 
        JOIN programa p 
        ON d.codigo = p.codigo 
        WHERE d.id_fab=(
          SELECT f.id_fab 
            FROM fabricante f 
            WHERE f.nombre='Oracle'));

-- 40 ejercicio

  SELECT c.nombre  FROM programa p   JOIN registra r   JOIN cliente c   ON p.codigo = r.codigo   AND r.dni = c.dni   WHERE p.nombre='Access'   AND p.version='XP';

-- 41 ejercicio

  -- C1
  SELECT f.pais 
    FROM fabricante f 
    WHERE f.nombre='Oracle';

  -- Consulta final
  SELECT f.nombre 
    FROM fabricante f 
    WHERE f.pais=(
      SELECT f.pais 
        FROM fabricante f 
        WHERE f.nombre='Oracle');

-- 42 ejercicio

  -- C1 - 
  SELECT c.edad
    FROM cliente c
    WHERE c.nombre='Pepe Pérez';

  -- Consulta final
  SELECT c.nombre 
    FROM cliente c 
    WHERE c.edad = (
      SELECT c.edad
        FROM cliente c
        WHERE c.nombre='Pepe Pérez');

-- 43 ejercicio.

  -- C1 - 
  SELECT c.ciudad 
    FROM comercio c 
    WHERE c.nombre='FNAC';

  -- Consulta final
  SELECT c.nombre 
    FROM comercio c 
    WHERE c.ciudad=(
      SELECT c.ciudad 
        FROM comercio c 
        WHERE c.nombre='FNAC');

-- 44 ejercicio

  -- C1 
  SELECT c.dni 
    FROM cliente c 
    WHERE c.nombre = 'Pepe Pérez';

  -- C2 - 
  SELECT DISTINCT r.medio 
    FROM cliente c 
    JOIN registra r
    ON c.dni = r.dni 
    WHERE r.dni = (
      SELECT c.dni 
        FROM cliente c 
        WHERE c.nombre = 'Pepe Pérez');

  -- Consulta final
  SELECT DISTINCT c.nombre 
    FROM cliente c 
    JOIN registra r 
    ON c.dni = r.dni 
    WHERE r.medio 
    IN (
      SELECT DISTINCT r.medio 
        FROM cliente c 
        JOIN registra r
        ON c.dni = r.dni 
        WHERE r.dni = (
          SELECT c.dni 
            FROM cliente c 
            WHERE c.nombre = 'Pepe Pérez'));

-- 45 ejercicio

  SELECT COUNT(*)programas  FROM programa p;

-- 46 ejercicio

  SELECT COUNT(*)clientes   FROM cliente c   WHERE c.edad>40;

-- 47 ejercicio

  SELECT SUM(d.cantidad)vendidos  FROM distribuye d  WHERE d.cif=1;

-- 48 ejercicio

  SELECT AVG(d.cantidad)media FROM distribuye d  WHERE d.codigo=7;

-- 49 ejercicio

  SELECT MIN(d.cantidad)minimo  FROM distribuye d  WHERE d.codigo=7;

-- 50 ejercicio

  SELECT MAX(d.cantidad)maximo   FROM distribuye d   WHERE d.codigo=7;

-- 51 ejercicio

  SELECT COUNT(*)establecimientos  FROM distribuye d  JOIN comercio c    ON d.cif = c.cif   WHERE d.codigo=7;

-- 52 ejercicio

  SELECT COUNT(*)registrosInternet  FROM registra r WHERE r.medio = 'Internet';

-- 53 ejercicio

  SELECT SUM(d.cantidad)ventasSevilla   FROM distribuye d   JOIN comercio c   ON d.cif = c.cif   WHERE c.ciudad = 'Sevilla';

-- 54 ejercicio

  SELECT COUNT(*)programas  FROM fabricante f  JOIN desarrolla d  ON f.id_fab = d.id_fab  WHERE f.pais='Estados Unidos'; 

-- 55 ejercicio

  SELECT UPPER(c.nombre)nombre, CHAR_LENGTH(c.nombre)caracteres   FROM cliente c;

-- 56 ejerccio

  SELECT CONCAT_WS(' ',p.nombre, p.version)programa  FROM programa p;

       


       












